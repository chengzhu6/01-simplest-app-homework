package com.twuc.webApp.entity;

import javax.validation.constraints.NotNull;

public class MathExpression {

    @NotNull
    private Long operandLeft;

    @NotNull
    private Long operandRight;

    @NotNull
    private String operation;

    @NotNull
    private Long expectedResult;

    private String checkType;

    public MathExpression() {
    }


    public MathExpression(Long operandLeft, Long operandRight, String operation, Long expectedResult) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
    }

    public MathExpression(@NotNull Long operandLeft, @NotNull Long operandRight, @NotNull String operation, @NotNull Long expectedResult, String checkType) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
        this.checkType = checkType;
    }

    public Long getOperandLeft() {
        return operandLeft;
    }

    public Long getOperandRight() {
        return operandRight;
    }

    public String getOperation() {
        return operation;
    }

    public Long getExpectedResult() {
        return expectedResult;
    }

    public String getCheckType() {
        return checkType;
    }
}
