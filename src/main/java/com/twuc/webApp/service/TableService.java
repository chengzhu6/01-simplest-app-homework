package com.twuc.webApp.service;

import com.twuc.webApp.common.Operation;
import com.twuc.webApp.entity.MathExpression;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


@Service
public class TableService {

    public String getTable(Enum operation , int start, int end) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int secondItem = start; secondItem <= end; secondItem++) {
            for (int firstItem = start; firstItem <= secondItem; firstItem++) {
                stringBuilder.append(getItem(firstItem, secondItem, operation));
            }
            stringBuilder.append("<br>");
        }

        return stringBuilder.toString();
    }



    private String getItem(int firstItem, int secondItem, Enum operation) {
        StringBuilder stringBuilder = new StringBuilder();
        if (Operation.PLUS.equals(operation)) {
            stringBuilder.append(firstItem).append("+").append(secondItem).append("=").append(firstItem + secondItem);
        } else {
            stringBuilder.append(firstItem).append("*").append(secondItem).append("=").append(firstItem * secondItem);
        }

        if (stringBuilder.toString().length() == 6) {
            stringBuilder.append("&nbsp;");
        } else {
            stringBuilder.append("&nbsp;&nbsp;&nbsp;");
        }
        return stringBuilder.toString();
    }


    public Map<String, Boolean> checkExpression(MathExpression mathExpression) {
        HashMap<String, Boolean> result = new HashMap<>();
        Boolean isCorrect;
        Long expressionResult;
        if ("+".equals(mathExpression.getOperation())) {
            expressionResult = mathExpression.getOperandLeft() + mathExpression.getOperandRight();
        } else {
            expressionResult = mathExpression.getOperandLeft() * mathExpression.getOperandRight();
        }
        int compareResult = expressionResult.compareTo(mathExpression.getExpectedResult());
        if (">".equals(mathExpression.getCheckType())) {
            isCorrect = compareResult > 0;
        } else if ("<".equals(mathExpression.getCheckType())) {
            isCorrect = compareResult < 0;
        } else {
            isCorrect = compareResult == 0;
        }

        result.put("correct", isCorrect);
        return result;
    }
}
