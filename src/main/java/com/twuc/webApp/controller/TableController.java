package com.twuc.webApp.controller;


import com.twuc.webApp.common.Operation;
import com.twuc.webApp.entity.MathExpression;
import com.twuc.webApp.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class TableController {

    @Autowired
    private TableService tableService;


    @GetMapping("/table/plus")
    public String getPlusTable(HttpServletResponse httpServletResponse, @RequestParam(defaultValue = "1") int start, @RequestParam(defaultValue = "9") int end) {
        httpServletResponse.setContentType("text/plain");
        return tableService.getTable(Operation.PLUS, start, end);
    }

    @GetMapping("/table/multiply")
    public String getMultiplyTable(HttpServletResponse httpServletResponse, @RequestParam(defaultValue = "1") int start, @RequestParam(defaultValue = "9") int end) {
        httpServletResponse.setContentType("text/plain");
        return tableService.getTable(Operation.MULTIPLY, start, end);
    }

    @PostMapping("/check")
    public Map<String, Boolean> checkTheExpression(@RequestBody @Valid MathExpression mathExpression) {
        return tableService.checkExpression(mathExpression);
    }
}
