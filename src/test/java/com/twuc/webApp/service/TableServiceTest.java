package com.twuc.webApp.service;


import com.twuc.webApp.common.Operation;
import com.twuc.webApp.entity.MathExpression;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TableServiceTest {

    @Test
    void should_return_plus_string_when_called_with_plus() {
        TableService tableService = new TableService();
        String table = tableService.getTable(Operation.PLUS, 1, 9);
        assertEquals(table, "1*1=1&nbsp;&nbsp;&nbsp;<br>" +
                "1*2=2&nbsp;&nbsp;&nbsp;2*2=4&nbsp;&nbsp;&nbsp;<br>" +
                "1*3=3&nbsp;&nbsp;&nbsp;2*3=6&nbsp;&nbsp;&nbsp;3*3=9&nbsp;&nbsp;&nbsp;<br>" +
                "1*4=4&nbsp;&nbsp;&nbsp;2*4=8&nbsp;&nbsp;&nbsp;3*4=12&nbsp;4*4=16&nbsp;<br>" +
                "1*5=5&nbsp;&nbsp;&nbsp;2*5=10&nbsp;3*5=15&nbsp;4*5=20&nbsp;5*5=25&nbsp;<br>" +
                "1*6=6&nbsp;&nbsp;&nbsp;2*6=12&nbsp;3*6=18&nbsp;4*6=24&nbsp;5*6=30&nbsp;6*6=36&nbsp;<br>" +
                "1*7=7&nbsp;&nbsp;&nbsp;2*7=14&nbsp;3*7=21&nbsp;4*7=28&nbsp;5*7=35&nbsp;6*7=42&nbsp;7*7=49&nbsp;<br>" +
                "1*8=8&nbsp;&nbsp;&nbsp;2*8=16&nbsp;3*8=24&nbsp;4*8=32&nbsp;5*8=40&nbsp;6*8=48&nbsp;7*8=56&nbsp;8*8=64&nbsp;<br>" +
                "1*9=9&nbsp;&nbsp;&nbsp;2*9=18&nbsp;3*9=27&nbsp;4*9=36&nbsp;5*9=45&nbsp;6*9=54&nbsp;7*9=63&nbsp;8*9=72&nbsp;9*9=81&nbsp;<br>");
    }

    @Test
    void should_return_multiply_string_when_called_with_multiply() {
        TableService tableService = new TableService();
        String table = tableService.getTable(Operation.MULTIPLY, 1, 9);
        assertEquals(table, "1*1=1&nbsp;&nbsp;&nbsp;<br>" +
                "1*2=2&nbsp;&nbsp;&nbsp;2*2=4&nbsp;&nbsp;&nbsp;<br>" +
                "1*3=3&nbsp;&nbsp;&nbsp;2*3=6&nbsp;&nbsp;&nbsp;3*3=9&nbsp;&nbsp;&nbsp;<br>" +
                "1*4=4&nbsp;&nbsp;&nbsp;2*4=8&nbsp;&nbsp;&nbsp;3*4=12&nbsp;4*4=16&nbsp;<br>" +
                "1*5=5&nbsp;&nbsp;&nbsp;2*5=10&nbsp;3*5=15&nbsp;4*5=20&nbsp;5*5=25&nbsp;<br>" +
                "1*6=6&nbsp;&nbsp;&nbsp;2*6=12&nbsp;3*6=18&nbsp;4*6=24&nbsp;5*6=30&nbsp;6*6=36&nbsp;<br>" +
                "1*7=7&nbsp;&nbsp;&nbsp;2*7=14&nbsp;3*7=21&nbsp;4*7=28&nbsp;5*7=35&nbsp;6*7=42&nbsp;7*7=49&nbsp;<br>" +
                "1*8=8&nbsp;&nbsp;&nbsp;2*8=16&nbsp;3*8=24&nbsp;4*8=32&nbsp;5*8=40&nbsp;6*8=48&nbsp;7*8=56&nbsp;8*8=64&nbsp;<br>" +
                "1*9=9&nbsp;&nbsp;&nbsp;2*9=18&nbsp;3*9=27&nbsp;4*9=36&nbsp;5*9=45&nbsp;6*9=54&nbsp;7*9=63&nbsp;8*9=72&nbsp;9*9=81&nbsp;<br>");
    }


    @Test
    void should_return_true_when_check_the_expression_is_true() {
        MathExpression mathExpression = new MathExpression(1L, 2L, "+", 3L);
        TableService tableService = new TableService();
        Map<String, Boolean> stringBooleanMap = tableService.checkExpression(mathExpression);
        assertEquals(stringBooleanMap.get("correct"), true);
    }

    @Test
    void should_return_false_when_check_the_expression_is_false() {
        MathExpression mathExpression = new MathExpression(1L, 2L, "+", 4L);
        TableService tableService = new TableService();
        Map<String, Boolean> stringBooleanMap = tableService.checkExpression(mathExpression);
        assertEquals(stringBooleanMap.get("correct"), false);
    }

    @Test
    void should_return_false_when_check_the_expression_with_check_style_is_false() {
        MathExpression mathExpression = new MathExpression(1L, 2L, "+", 4L, "<");
        TableService tableService = new TableService();
        Map<String, Boolean> stringBooleanMap = tableService.checkExpression(mathExpression);
        assertEquals(stringBooleanMap.get("correct"), true);
    }
}