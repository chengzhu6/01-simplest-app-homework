package com.twuc.webApp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.entity.MathExpression;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
class TableControllerTest {
    @Autowired
    private MockMvc mockMvc;


    @Test
    void should_return_plus_table_when_request_plus_table() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/table/plus"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain"))
                .andExpect(MockMvcResultMatchers.content().string("1*1=1&nbsp;&nbsp;&nbsp;<br>" +
                        "1*2=2&nbsp;&nbsp;&nbsp;2*2=4&nbsp;&nbsp;&nbsp;<br>" +
                        "1*3=3&nbsp;&nbsp;&nbsp;2*3=6&nbsp;&nbsp;&nbsp;3*3=9&nbsp;&nbsp;&nbsp;<br>" +
                        "1*4=4&nbsp;&nbsp;&nbsp;2*4=8&nbsp;&nbsp;&nbsp;3*4=12&nbsp;4*4=16&nbsp;<br>" +
                        "1*5=5&nbsp;&nbsp;&nbsp;2*5=10&nbsp;3*5=15&nbsp;4*5=20&nbsp;5*5=25&nbsp;<br>" +
                        "1*6=6&nbsp;&nbsp;&nbsp;2*6=12&nbsp;3*6=18&nbsp;4*6=24&nbsp;5*6=30&nbsp;6*6=36&nbsp;<br>" +
                        "1*7=7&nbsp;&nbsp;&nbsp;2*7=14&nbsp;3*7=21&nbsp;4*7=28&nbsp;5*7=35&nbsp;6*7=42&nbsp;7*7=49&nbsp;<br>" +
                        "1*8=8&nbsp;&nbsp;&nbsp;2*8=16&nbsp;3*8=24&nbsp;4*8=32&nbsp;5*8=40&nbsp;6*8=48&nbsp;7*8=56&nbsp;8*8=64&nbsp;<br>" +
                        "1*9=9&nbsp;&nbsp;&nbsp;2*9=18&nbsp;3*9=27&nbsp;4*9=36&nbsp;5*9=45&nbsp;6*9=54&nbsp;7*9=63&nbsp;8*9=72&nbsp;9*9=81&nbsp;<br>"));
    }

    @Test
    void should_return_multiply_table_when_request_multiply_table() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/table/multiply"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain"))
                .andExpect(MockMvcResultMatchers.content().string("1*1=1&nbsp;&nbsp;&nbsp;<br>" +
                        "1*2=2&nbsp;&nbsp;&nbsp;2*2=4&nbsp;&nbsp;&nbsp;<br>" +
                        "1*3=3&nbsp;&nbsp;&nbsp;2*3=6&nbsp;&nbsp;&nbsp;3*3=9&nbsp;&nbsp;&nbsp;<br>" +
                        "1*4=4&nbsp;&nbsp;&nbsp;2*4=8&nbsp;&nbsp;&nbsp;3*4=12&nbsp;4*4=16&nbsp;<br>" +
                        "1*5=5&nbsp;&nbsp;&nbsp;2*5=10&nbsp;3*5=15&nbsp;4*5=20&nbsp;5*5=25&nbsp;<br>" +
                        "1*6=6&nbsp;&nbsp;&nbsp;2*6=12&nbsp;3*6=18&nbsp;4*6=24&nbsp;5*6=30&nbsp;6*6=36&nbsp;<br>" +
                        "1*7=7&nbsp;&nbsp;&nbsp;2*7=14&nbsp;3*7=21&nbsp;4*7=28&nbsp;5*7=35&nbsp;6*7=42&nbsp;7*7=49&nbsp;<br>" +
                        "1*8=8&nbsp;&nbsp;&nbsp;2*8=16&nbsp;3*8=24&nbsp;4*8=32&nbsp;5*8=40&nbsp;6*8=48&nbsp;7*8=56&nbsp;8*8=64&nbsp;<br>" +
                        "1*9=9&nbsp;&nbsp;&nbsp;2*9=18&nbsp;3*9=27&nbsp;4*9=36&nbsp;5*9=45&nbsp;6*9=54&nbsp;7*9=63&nbsp;8*9=72&nbsp;9*9=81&nbsp;<br>"));
    }


    @Test
    void should_return_true_when_check_the_correct_expression() throws Exception {

        MathExpression mathExpression = new MathExpression(1L, 2L, "+", 3L);
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(mathExpression);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(json))
                .andExpect(MockMvcResultMatchers.jsonPath("$.correct").value(true));
    }


    @Test
    void should_return_false_when_check_the_incorrect_expression() throws Exception {

        MathExpression mathExpression = new MathExpression(1L, 2L, "+", 4L);
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(mathExpression);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(json))
                .andExpect(MockMvcResultMatchers.jsonPath("$.correct").value(false));
    }



    @Test
    void should_return_400_when_without_any_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content("{\"operandRight\":2,\"operation\":\"+\",\"expectedResult\":4}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
    @Test
    void should_return_true_when_check_the_incorrect_expression_with_check_style() throws Exception {

        MathExpression mathExpression = new MathExpression(1L, 2L, "+", 4L, "<");
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(mathExpression);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(json))
                .andExpect(MockMvcResultMatchers.jsonPath("$.correct").value(true));
    }

    @Test
    void should_return_false_when_check_the_incorrect_expression_with_check_style() throws Exception {

        MathExpression mathExpression = new MathExpression(1L, 2L, "+", 1L, "<");
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(mathExpression);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(json))
                .andExpect(MockMvcResultMatchers.jsonPath("$.correct").value(false));
    }
}